# Spring App for structuring a writer interface

Tech Strategy:
```sh
Framework: Spring boot
Plugins: Lombok
Test-suite: Jupiter (JUnit)
```

Writers can be of multiple types and extendable. (e.g., StringWriter, FileWriter)

Operators can also be of multiple types and extendable. (e.g., Uppercase, Duplicate remover)

Install it and run:

```sh
mvn install
mvn spring-boot:run
```


Run test cases:

```sh
mvn test
```
