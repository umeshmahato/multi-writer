package com.umesh.writer.service;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 11:34 AM
 */

@Service
public interface WriterService {

    public void init();
    public void write(String string) throws IOException;
    public String read();
    public void close();
}
