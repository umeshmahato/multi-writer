package com.umesh.writer.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 3:16 PM
 */

@Service
public interface OperatorService {

    default String lowerCase(String str) {
        return str != null ? str.toLowerCase() : "";
    }

    default String upperCase(String str) {
        return str != null ? str.toUpperCase() : "";
    }

    default String stupidRemover(String str) {
        if (str == null) return "";
        return str.replace("stupid", "s*****");
    }

    default String duplicateRemover(String str) {
        if (str == null) return "";
        String[] arr = str.split(" ");
        List<String> processedArr = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            if (i == 0) processedArr.add(arr[i]);
            else {
                if (!Objects.equals(arr[i], arr[i - 1])) processedArr.add(arr[i]);
            }
        }
        return String.join(" ", processedArr);
    }

}
