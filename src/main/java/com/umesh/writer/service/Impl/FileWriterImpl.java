package com.umesh.writer.service.Impl;

import com.umesh.writer.service.OperatorService;
import com.umesh.writer.service.WriterService;
import lombok.Getter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 5:34 PM
 */

@Component
@Getter
public class FileWriterImpl implements WriterService, OperatorService {

    private static final Log LOGGER = LogFactory.getLog(FileWriterImpl.class);
    private static final String filePath = "file.txt";
    private FileWriter fileWriter;
    private FileReader fileReader;

    @Override
    public void init() {
        try {
            fileWriter = new FileWriter(filePath);
            fileReader = new FileReader(filePath);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void write(String string) throws IOException {
        fileWriter.write(string);
    }

    @Override
    public String read() {
        try {
            StringBuilder str = new StringBuilder();
            int character;
            while ((character = fileReader.read()) != -1) {
                str.append((char) character);
            }
            return str.toString();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return "";
        }
    }

    @Override
    public void close() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
