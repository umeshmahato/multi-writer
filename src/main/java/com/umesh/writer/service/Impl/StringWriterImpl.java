package com.umesh.writer.service.Impl;

import com.umesh.writer.service.OperatorService;
import com.umesh.writer.service.WriterService;
import lombok.Getter;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 11:41 AM
 */

@Component
@Getter
@Primary
public class StringWriterImpl implements WriterService, OperatorService {

    private String stringContent;
    private boolean closed;

    @Override
    public void init() {
        this.stringContent = "";
        this.closed = false;
    }

    @Override
    public void write(String string) {
        if (!this.closed) stringContent += string;
    }

    @Override
    public String read() {
        return stringContent;
    }

    @Override
    public void close() {
        this.closed = true;
    }
}
