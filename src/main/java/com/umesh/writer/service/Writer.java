package com.umesh.writer.service;

import com.umesh.writer.Helper.OperatorENUM;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * @author umesh.mahato@landmarkgroup.com
 * @project writer
 * @created 07/08/2022 - 11:42 AM
 */

@Component
@Getter
@Setter
public class Writer implements OperatorService {

    public WriterService writer;

    public Writer(WriterService writerService) {
        this.writer = writerService;
    }

    public void init() {
        this.writer.init();
    }

    public void write(String str, List<OperatorENUM> operators) throws IOException {
        String content = str;
        for (OperatorENUM operator : operators) {
            switch (operator) {
                case LOWERCASE:
                    content = this.lowerCase(content);
                    break;
                case UPPERCASE:
                    content = this.upperCase(content);
                    break;
                case STUPID_REMOVER:
                    content = this.stupidRemover(content);
                    break;
                case DUPLICATE_REMOVER:
                    content = this.duplicateRemover(content);
                    break;
                default:
                    break;
            }
        }
        this.writer.write(content);
    }

    public String print() {
        return this.writer.read();
    }

    public void close() {
        this.writer.close();
    }

}
