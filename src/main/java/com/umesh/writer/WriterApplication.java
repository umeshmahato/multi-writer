package com.umesh.writer;

import com.umesh.writer.Helper.OperatorENUM;
import com.umesh.writer.service.Impl.FileWriterImpl;
import com.umesh.writer.service.Impl.StringWriterImpl;
import com.umesh.writer.service.Writer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 08/08/2022 - 10:34 PM
 */

@SpringBootApplication
public class WriterApplication {

    private static final Log LOGGER = LogFactory.getLog(WriterApplication.class);

	public static void main(String[] args) throws IOException {
        SpringApplication.run(WriterApplication.class, args);
		String testString = "This is really really stupid!!!";

		try {
//			StringWriter: writing with operator UPPERCASE
            StringWriterImpl stringWriter = new StringWriterImpl();
            Writer writer = new Writer(stringWriter);
            writer.init();
			writer.write(testString, Collections.singletonList(OperatorENUM.UPPERCASE));
            writer.close();
            System.out.println(writer.print());

//			FileWriter: writing with operator DUPLICATE_REMOVER and STUPID_REMOVER
            FileWriterImpl fileWriter = new FileWriterImpl();
            writer = new Writer(fileWriter);
            writer.init();
            writer.write(testString, Arrays.asList(OperatorENUM.DUPLICATE_REMOVER, OperatorENUM.STUPID_REMOVER));
            writer.close();
            System.out.println(writer.print());

//			FileWriter: Attempting to write again after file connections is closed
            writer.write("Alternate text", new ArrayList<>());
            writer.close();
            System.out.println(writer.print());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

    }

    @Bean
    public WebMvcConfigurer crossOriginConfig() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*").allowedMethods("*");
            }
        };
    }

}
