package com.umesh.writer.Helper;


/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 3:42 PM
 */

public enum OperatorENUM {

    LOWERCASE,
    UPPERCASE,
    STUPID_REMOVER,
    DUPLICATE_REMOVER

}
