package com.umesh.writer;

import com.umesh.writer.Helper.OperatorENUM;
import com.umesh.writer.service.Impl.FileWriterImpl;
import com.umesh.writer.service.Writer;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 10:48 PM
 */

@SpringBootTest
public class FileWriterTests {

    private static String testString;
    private Writer writer;

    @BeforeAll
    static void setup() {
        testString = "This is really really stupid!!!";
    }

    @BeforeEach
    void setupWriter() {
        FileWriterImpl fileWriter = new FileWriterImpl();
        writer = new Writer(fileWriter);
        writer.init();
    }

    @Test
    @DisplayName("No Operators are applied before writing to file")
    void noOperators() throws IOException {
        writer.write(testString, new ArrayList<>());
        writer.close();
        String output = writer.print();
        assertEquals("This is really really stupid!!!", output);
    }

    @Test
    @DisplayName("Uppercase Operator is applied before writing to file")
    void lowerCaseOperator() throws IOException {
        List<OperatorENUM> operators = Collections.singletonList(OperatorENUM.UPPERCASE);
        writer.write(testString, operators);
        writer.close();
        String output = writer.print();
        assertEquals("THIS IS REALLY REALLY STUPID!!!", output);
    }

    @Test
    @DisplayName("Duplicate and Stupid Removal Operators are applied before writing to file")
    void duplicateAndStupidOperator() throws IOException {
        List<OperatorENUM> operators = Arrays.asList(OperatorENUM.DUPLICATE_REMOVER, OperatorENUM.STUPID_REMOVER);
        writer.write(testString, operators);
        writer.close();
        String output = writer.print();
        assertEquals("This is really s*****!!!", output);
    }

    @Test
    @DisplayName("Attempting to write after closing file writer connection")
    void afterClose() {
        try {
            writer.write(testString, new ArrayList<>());
            writer.close();

            String output = writer.print();
            assertEquals("This is really really stupid!!!", output);

            writer.write("Attempting to write again", new ArrayList<>());
        } catch (IOException e) {
            assertEquals("Stream closed", e.getMessage());
        }
    }
}
