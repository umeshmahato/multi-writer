package com.umesh.writer;

import com.umesh.writer.Helper.OperatorENUM;
import com.umesh.writer.service.Impl.StringWriterImpl;
import com.umesh.writer.service.Writer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author toumesh@outlook.com
 * @project writer
 * @created 07/08/2022 - 08:28 PM
 */

@SpringBootTest
class StringWriterTests {

    private static final Log LOGGER = LogFactory.getLog(StringWriterTests.class);

    private static String testString;
	private Writer writer;

    @BeforeAll
    static void setup() {
        testString = "This is really really stupid!!!";
    }

	@BeforeEach
	void setupWriter() {
		StringWriterImpl stringWriter = new StringWriterImpl();
		writer = new Writer(stringWriter);
		writer.init();
	}

    @Test
    @DisplayName("No Operators are applied before writing to string")
    void noOperators() throws IOException {
        writer.write(testString, new ArrayList<>());
        String output = writer.print();
        assertEquals("This is really really stupid!!!", output);
    }

    @Test
    @DisplayName("Lowercase Operator is applied before writing to string")
    void lowerCaseOperator() throws IOException {
        List<OperatorENUM> operators = Collections.singletonList(OperatorENUM.LOWERCASE);
        writer.write(testString, operators);
        String output = writer.print();
        assertEquals("this is really really stupid!!!", output);
    }

    @Test
    @DisplayName("Duplicate and Stupid Removal Operators are applied before writing to string")
    void duplicateAndStupidOperator() throws IOException {
        List<OperatorENUM> operators = Arrays.asList(OperatorENUM.DUPLICATE_REMOVER, OperatorENUM.STUPID_REMOVER);
        writer.write(testString, operators);
        String output = writer.print();
        assertEquals("This is really s*****!!!", output);
    }

    @Test
    @DisplayName("Uppercase, Duplicate and Stupid Removal Operators are applied before writing to string")
    void upperCaseDuplicateAndStupidOperator() throws IOException {
        List<OperatorENUM> operators = Arrays.asList(OperatorENUM.UPPERCASE,
                OperatorENUM.DUPLICATE_REMOVER,
                OperatorENUM.STUPID_REMOVER);
        writer.write(testString, operators);
        String output = writer.print();
        assertEquals("THIS IS REALLY STUPID!!!", output);
    }

}
